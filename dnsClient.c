#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <poll.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdlib.h>
#include <pthread.h>

#include "types.h"
#include "err.h"

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "helpers.h"

#define FROM_SEC 1000
#define LISTENER_NUM 1

void partitionDns(int* index, char* buf, const char* data)
{
    buf[(*index)++] = strlen(data);
    memcpy(buf + *index, data, strlen(data));
    (*index) += strlen(data);
}

char* createQueryA(int* index, const char* name, const char* isSSH,
        const char* commType)
{
    char* buf = (char*) malloc(DNS_SIZE);
    struct DNS_HEADER *dns = (struct DNS_HEADER *)buf;
    dns->id = 0;
    dns->qr = 0;
    dns->opcode = 0;
    dns->aa = 0;
    dns->tc = 0;
    dns->rd = 1;
    dns->ra = 0;
    dns->z = 0;
    dns->ad = 0;
    dns->cd = 0;
    dns->rcode = 0;
    dns->q_count = htons(1);
    dns->ans_count = 0;
    dns->auth_count = 0;
    dns->add_count = 0;

    *index = sizeof(struct DNS_HEADER);

    partitionDns(index, buf, name);
    partitionDns(index, buf, isSSH);
    partitionDns(index, buf, commType);
    partitionDns(index, buf, "local");

    buf[(*index)++] = 0;

    struct QUESTION *quest = (struct QUESTION *)(buf+*index);
    quest->qtype = htons(A);
    quest->qclass = htons(1);
    (*index) = *index + sizeof(struct QUESTION);
    return buf;
}

char* createQueryPTR(int* index, const char* isSSH,
        const char* commType)
{
    char* buf = (char*) malloc(DNS_SIZE);
    struct DNS_HEADER *dns = (struct DNS_HEADER *)buf;
    dns->id = 0;
    dns->qr = 0;
    dns->opcode = 0;
    dns->aa = 0;
    dns->tc = 0;
    dns->rd = 1;
    dns->ra = 0;
    dns->z = 0;
    dns->ad = 0;
    dns->cd = 0;
    dns->rcode = 0;
    dns->q_count = htons(1);
    dns->ans_count = 0;
    dns->auth_count = 0;
    dns->add_count = 0;
    *index = sizeof(struct DNS_HEADER);
    partitionDns(index, buf, isSSH);
    partitionDns(index, buf, commType);
    partitionDns(index, buf, "local");
    buf[(*index)++] = 0;
    struct QUESTION *q = (struct QUESTION *)(buf+*index);
    q->qtype = htons(PTR);
    q->qclass = htons(1);
    (*index) = *index + sizeof(struct QUESTION);
    return buf;
}

int readAns(const char* buf, const int len, int* rdataName,
        int* rdataNameLen)
{
    int qname_parts = 0, i, j;
    char qname[NAME_PARTS][NAME_LEN];

    for (i = 0; i < NAME_PARTS; i++)
    {
        for(j=0; j < NAME_LEN; j++)
            qname[i][j] = 0;
    }

    int index = sizeof(struct DNS_HEADER);
    while(1)
    {
        if(index >= len)
            break;
        if (buf[index] == 0)
            break;
        if(index + buf[index] >= len || buf[index] >= NAME_LEN)
            break;
        memcpy(qname[qname_parts++], buf + index + 1, buf[index]);
        index += buf[index];
        index++;
    }
    struct R_DATA* r = (struct R_DATA*) (buf + index + 1);
    int qtype = ntohs(r->type);
    (*rdataNameLen) = ntohs(r->data_len);
    index += sizeof(struct R_DATA);
    (*rdataName) = index - 1;

    short isOp, isUdp, isSsh, isLocal, isTcp;

    if (qname_parts == 3 && qtype == PTR)
    {
        isOp = (strcmp(qname[0], "_opoznienia") == 0);
        isUdp = (strcmp(qname[1], "_udp") == 0);
        isLocal = (strcmp(qname[2], "local") == 0);
        isSsh = (strcmp(qname[0], "_ssh") == 0); //best variable name
        isTcp = (strcmp(qname[1], "_tcp") == 0);

        if (isSsh && isLocal && isTcp)
            return SSH_ANSWER_PTR;
        else if (isOp && isUdp && isLocal)
            return UDP_ANSWER_PTR;
    }

    else if(qname_parts == 4 && qtype == A)
    {
        isOp = (strcmp(qname[1], "_opoznienia") == 0);
        isUdp = (strcmp(qname[2], "_udp") == 0);
        isLocal = (strcmp(qname[3], "local") == 0);
        isSsh = (strcmp(qname[1], "_ssh") == 0);
        isTcp = (strcmp(qname[2], "_tcp") == 0);

        if (isSsh && isLocal && isTcp)
            return SSH_ANSWER_A;
        else if (isOp && isUdp && isLocal)
            return UDP_ANSWER_A;
    }

    return UNKNOWN;
}

void * dnsClient(void * arg)
{
    printf("DNS Cli startup\n");
    struct dnsCliArgs args = *(struct dnsCliArgs *) arg;
    pthread_mutex_t* protect = args.mutexArray;
    struct nodeInfo* nodesArray = args.nodesArray;

    int len;
    int port = DNS_PORT, sock = socket(PF_INET, SOCK_DGRAM, 0),
        searchDelay = args.scanInterval * FROM_SEC;
    int* lastNode = args.countHosts;

    struct timeval prev, timeNow;
    struct pollfd listener[LISTENER_NUM];
    int i;

    gettimeofday(&prev, NULL);

    listener[0].fd = sock;
    listener[0].events = POLLIN;
    listener[0].revents = 0;

    while (1)
    {
        gettimeofday(&timeNow, NULL);
        if (poll(listener, LISTENER_NUM, searchDelay -
                    timeDiff(prev, timeNow)) == -1)
            syserr("poll");
        gettimeofday(&timeNow, NULL);

        if (searchDelay <= timeDiff(prev, timeNow))
        {
            gettimeofday(&prev, NULL);
            char * mesg = createQueryPTR(&len, "_ssh","_tcp");
            sendDNS(sock, port, mesg, len);
            free(mesg);

            mesg = createQueryPTR(&len, "_opoznienia","_udp");
            sendDNS(sock, port, mesg, len);
            free(mesg);
        }

        if(listener[0].revents & POLLIN)
        {
            listener[0].revents = 0;
            char dns[DNS_SIZE];
            len = recv(sock, dns, DNS_SIZE, 0);
            if (len == -1)
                syserr("recv");
            int rdName, rdLen;
            int mesg = readAns(dns, len, &rdName, &rdLen);

            if (mesg == SSH_ANSWER_A)
            {
                int* ip = (struct in_addr*)(dns + rdName);
                printf("DNS Cli SSH A %d\n", ip);

                for(i = 0; i < *lastNode; i++)
                {
                    if (pthread_mutex_lock(&protect[i]) != 0)
                        fatal("pthread_mutex_lock");

                    if(nodesArray[i].ip == *ip)
                    {
                        nodesArray[i].tcp = 0;
                        if (pthread_mutex_unlock(&protect[i]) != 0)
                            fatal("pthread_mutex_unlock");
                        break;
                    }

                    if (pthread_mutex_unlock(&protect[i]) != 0)
                        fatal("pthread_mutex_unlock");
                }
                if (pthread_mutex_lock(&protect[i]) != 0)
                    fatal("pthread_mutex_lock");

                if(i >= *lastNode && *lastNode < N)
                {
                    nodesArray[*lastNode].ip = *ip;
                    nodesArray[*lastNode].tcp = 0;
                    (*lastNode) = *lastNode + 1;
                }

                if (pthread_mutex_unlock(&protect[i]) != 0)
                    fatal("pthread_mutex_unlock");
            }
            else if (mesg == SSH_ANSWER_PTR)
            {
                char* name = (char*) malloc(rdLen + 1);
                name[rdLen] = 0;
                name = (char*) memcpy(name, dns+rdName, rdLen);

                printf("DNS Cli SSH PTR %s\n",name);
                
                char* mesg = createQueryA(&len, name, "_ssh", "_tcp");
                sendDNS(sock, port, mesg, len);
                free(mesg);
            }
            else if (mesg == UDP_ANSWER_A)
            {
                int* ip = (struct in_addr*)(dns + rdName);
                printf("DNS Cli UDP A %d\n", *ip);
                
                for(i = 0; i < *lastNode; i++)
                {
                    if (pthread_mutex_lock(&protect[i]) != 0)
                        fatal("pthread_mutex_lock");
                    
                    if(nodesArray[i].ip == *ip)
                    {
                        if (pthread_mutex_unlock(&protect[i]) != 0)
                            fatal("pthread_mutex_unlock");
                        break;
                    }
                    
                    if (pthread_mutex_unlock(&protect[i]) != 0)
                        fatal("pthread_mutex_unlock");
                }

                if (pthread_mutex_lock(&protect[i]) != 0)
                    fatal("pthread_mutex_lock");

                if(*lastNode < N && i >= *lastNode)
                {
                    nodesArray[*lastNode].ip = *ip;
                    nodesArray[*lastNode].tcp = 1;
                    (*lastNode) = *lastNode + 1;
                }

                if (pthread_mutex_unlock(&protect[i]) != 0)
                    fatal("pthread_mutex_unlock");
            }
            else if (mesg == UDP_ANSWER_PTR)
            {
                char* name = (char*) malloc(rdLen + 1);
                name[rdLen] = 0;
                name = (char*) memcpy(name, dns + rdName, rdLen);
                printf("DNS Cli UDP PTR %s\n",name);
                
                char* mesg = createQueryA(&len, name, "_opoznienia", "_udp");
                sendDNS(sock, port, mesg, len);
                free(mesg);
            }
            else
                printf("DNS Cli strange mesg\n");
            listener[0].revents = 0;
        }
    }
}
