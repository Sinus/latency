#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <endian.h>
#include <pthread.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <poll.h>
#include <sys/time.h>
#include <sys/types.h>
#include <inttypes.h>
#include <string.h>
#include <stdint.h>
#include <netinet/ip_icmp.h>

#include "err.h"
#include "helpers.h"
#include "types.h"
#include "hostUdpLag.h"
#include "dropnobody.h"
#include "in_cksum.h"
#include "dnsServer.h"
#include "dnsClient.h"

#define CONSOLE_HEIGHT 23
#define CONSOLE_WIDTH 80
#define TELNET_USERS 10
#define BSIZE 100
#define BUF_SIZE 1024
#define ICMP_HEADER_LEN 8

const int MICRO = 1000000;
const int threadNumber = 5;
const int tcpPort = 22;

int lastTakenNode = 0;
uint16_t icmpSeq;
int rawSock;

uint16_t sentIcmp[N]; //seq of sent icmp
uint64_t icmpTime[N]; //time when given icmp was sent
int icmpIndex[N]; //index in nodes[]/lags[] sent
pthread_mutex_t icmpMutex;

struct nodeInfo nodes[N];
struct lagInfo lags[N];
pthread_mutex_t mutex[N];

int findIcmpIndex(uint16_t x)
{
    int i;

    for (i = 0; i < N; i++)
    {
        if (sentIcmp[i] == x)
            return i;
    }

    return -1;
}

int findFreeIcmpIndex()
{
    int i;

    for (i = 0; i < N; i++)
    {
        if (icmpIndex[i] == -1)
            return i;
    }
    return -1;
}

/*uint64_t timeDiff(struct timeval before, struct timeval after)
{
    uint64_t bef = (uint64_t) before.tv_sec * MICRO + (uint64_t) before.tv_usec;
    uint64_t aft = (uint64_t) after.tv_sec * MICRO + (uint64_t) after.tv_usec;

    if (aft > bef) //in case someone forgets the order...
        return aft - bef;
    return bef - aft;
}*/

int averageLag(int i, short type)
{
    //type: 0 - udp, 1 - tcp, 2 - icmp
    //don't have to worry about mutex, we are protected by the caller

    int filledCells = 0;
    int sum = 0, j;

    if (type == 0)
    {
        for (j = 0; j < LAG_CACHE; j++)
        {
            if (lags[i].udp[j] > 0)
            {
                filledCells++;
                sum += lags[i].udp[j];
            }
        }
    }
    else if (type == 1)
    {
        for (j = 0; j < LAG_CACHE; j++)
        {
            if (lags[i].tcp[j] > 0)
            {
                filledCells++;
                sum += lags[i].tcp[j];
            }
        }
    }
    else if (type == 2)
    {
        for (j = 0; j < LAG_CACHE; j++)
        {
            if (lags[i].icmp[j] > 0)
            {
                filledCells++;
                sum += lags[i].icmp[j];
            }
        }
    }
    else
        fatal("wrong argument for average lag");
    return sum / filledCells;
}

void init()
{
    int i, j;
    rawSock = socket(PF_INET, SOCK_RAW, IPPROTO_ICMP);
    if (rawSock < 0)
        syserr("socket");
    for (i = 0; i < N; i++)
    {
        sentIcmp[i] = 0;
        icmpTime[i] = 0;
        icmpIndex[i] = -1;
        nodes[i].ip = -1;
        nodes[i].tcp = 0;
        lags[i].ip = -1;
        lags[i].icmpI = 0;
        lags[i].udpI = 0;
        lags[i].tcpI = 0;
        if (pthread_mutex_init(&mutex[i], 0) != 0)
            syserr("pthread_mutex_init");
        for (j = 0; j < 10; j++)
        {
            lags[i].tcp[j] = -1;
            lags[i].udp[j] = -1;
            lags[i].icmp[j] = -1;
        }
    }
    if (pthread_mutex_init(&icmpMutex, 0) != 0)
        syserr("pthread_mutex_init");
}

void setFlags(int argc, char** argv, struct parameters* params)
{
    int opt;

    params->udpLatencyPort = 3382;
    params->uiPort = 3637;
    params->checkLagDelay = 1;
    params->searchDelay = 10;
    params->uiDelay = 1;
    params->sshTcpOn = 0;

    while ((opt = getopt(argc, argv, "u:U:t:T:v:s")))
    {
        switch (opt)
        {
            case 'u':
                params->udpLatencyPort = atoi(optarg);
                break;
            case 'U':
                params->uiPort = atoi(optarg);
                break;
            case 't':
                params->checkLagDelay = atoi(optarg);
                break;
            case 'T':
                params->searchDelay = atoi(optarg);
                break;
            case 'v':
                params->uiDelay = atoi(optarg);
                break;
            case 's':
                params->sshTcpOn = 1;
                break;
            case -1:
                return; //no more args
            default: //unknown parameter
                fatal("Wrong arguments");
        }
    }
}

void UIServ(void * args)
{
    struct parameters params = *((struct parameters *) args);

    struct sockaddr_in server, clientAddr;
    size_t length;
    int shortestWait = params.uiDelay; //this should be better
    int ret, i, j, msgsock, clientCount = TELNET_USERS, active = 0;
    struct pollfd client[TELNET_USERS];
    struct uiClient connected[TELNET_USERS];
    ssize_t rval;
    char buf[BUF_SIZE];
    struct line lines[N];
    struct clientView views[TELNET_USERS];
    struct timeval timeNow, lastUpdate;

    for (i = 0; i < N; i++)
        lines[i].ip = -1;

    for (i = 0; i < clientCount; i++)
    {
        client[i].fd = -1;
        client[i].events = POLLIN;
        client[i].revents = 0;

        connected[i].fd = -1;
        connected[i].active = 0;
        connected[i].lastWrote = 0;

        views[i].writeFrom = 0;
    }


    /*for (i = 0; i < 40; i++)
    {
        lines[i].ip = i;
        lines[i].tcp = i;
        lines[i].udp = i;
        lines[i].icmp = i;
    }*/

    client[0].fd = socket(PF_INET, SOCK_STREAM, 0);
    if (client[0].fd < 0)
        syserr("socket");

    server.sin_family = AF_INET;
    server.sin_addr.s_addr = htonl(INADDR_ANY);
    server.sin_port = htons(params.uiPort);

    if (bind(client[0].fd, (struct sockaddr *) &server,
                (socklen_t) sizeof(server)) < 0)
        syserr("bind");

    length = sizeof(server);

    if (listen(client[0].fd, 5) == -1)
        syserr("listen");

    while (1)
    {
        gettimeofday(&timeNow, NULL);
        if (timeDiff(lastUpdate, timeNow) >= params.uiDelay * MICRO)
        {
            //printf("UPDATE WYSWIETLANYCH LINII\n");
            int lineIter = 0;
            //update lines
            for (i = 0; i < N; i++)
            {
                if (pthread_mutex_lock(&mutex[i]) != 0)
                    syserr("pthread_mutex_lock");
                lines[i].ip = nodes[i].ip;
                if (nodes[i].ip != 0)
                {
                    lines[i].udp = averageLag(i, 0);
                    lines[i].tcp = averageLag(i, 1);
                    lines[i].icmp = averageLag(i, 2);
                }

                if (pthread_mutex_unlock(&mutex[i]) != 0)
                    syserr("pthread_mutex_unlock");
                gettimeofday(&lastUpdate, NULL);
            }
            //printf("zrobilem linie\n");
        }

        for (j = 1; j < clientCount; j++)
        {
            if (connected[j].active == 1)
            {
                gettimeofday(&timeNow, NULL);
                uint64_t now = ((uint64_t) timeNow.tv_sec * MICRO +
                        (uint64_t) timeNow.tv_usec); //- connected[j].lastWrote;
                uint64_t diff = now - connected[j].lastWrote;

                if (diff >= (params.uiDelay * MICRO))
                {
                    client[j].events = (POLLOUT | POLLIN);
                }
            }
        }

        ret = poll(client, TELNET_USERS, shortestWait);
        if (ret < 0)
            syserr("poll");
        else if (ret >= 0)
        {
            if (client[0].fd != -1 && (client[0].revents & POLLIN))
            {
                client[0].revents = 0;
                msgsock = accept(client[0].fd, (struct sockaddr *) 0,
                        (socklen_t *) 0);
                if (msgsock == -1)
                    syserr("accept");
                else
                {
                    active++;
                    j = 1;
                    while (connected[j].active == 1)
                        j++;
                    connected[j].active = 1;
                    connected[j].fd = msgsock;
                    client[j].fd = msgsock;
                }
            }
            //read stuff
            for (i = 1; i < clientCount; i++)
            {
                if (client[i].fd != -1 && (client[i].revents & POLLIN))
                {
                    client[i].revents = 0;
                    rval = read(client[i].fd, buf, BUF_SIZE);
                    if (rval < 0)
                    {
                        if (close(client[i].fd) < 0)
                            syserr("close");
                        client[i].fd = -1;
                        active--;
                    }
                    else if (rval == 0)
                    {
                        if (close(client[i].fd) < 0)
                            syserr("close");
                        client[i].fd = -1;
                        active--;
                    }
                    else
                    {
                        int k, activeLines = 0;
                        for (k = 0; k < N; k++)
                            if (lines[k].ip != -1)
                                activeLines++;
                        if (activeLines > CONSOLE_HEIGHT)
                        {
                            if (buf[0] == 'q')
                            {
                                printf("PRZECZYTALEM Q\n");
                                if (views[i].writeFrom > 0)
                                    views[i].writeFrom--;
                                client[i].events = (POLLOUT | POLLIN);
                            }
                            else if (buf[0] == 'a')
                            {
                                printf("PRZECZYTALEM A\n");
                                if (activeLines - views[i].writeFrom > CONSOLE_HEIGHT)
                                    views[i].writeFrom++;
                                client[i].events = (POLLOUT | POLLIN);
                            }
                        }
                        else
                            client[i].events = (POLLOUT | POLLIN);
                    }
                }
            }
            //write to those who want
            for (i = 1; i < clientCount; i++)
            {
                if (client[i].fd != -1 && (client[i].revents & POLLOUT))
                {
                    client[i].revents = 0;
                    client[i].events = POLLIN;

                    //printf("WYSYLAM\n");
                    length = sprintf(buf, "\e[2J");
                    if (send(client[i].fd, buf, length, 0) == -1)
                        syserr("send");

                    int sent = 0;
                    for (j = views[i].writeFrom; j < N; j++)
                    {
                        if (lines[j].ip != -1)
                        {
                            if (sent >= CONSOLE_HEIGHT)
                                break;
                            sent++;

                            struct in_addr ipA;
                            char str[INET_ADDRSTRLEN];
                            ipA.s_addr = lines[j].ip;
                            inet_ntop(AF_INET, &ipA, str, INET_ADDRSTRLEN);

                            length = sprintf(buf, "%s udp: %d tcp: %d icmp: %d\n",
                                    str, lines[j].udp, lines[j].tcp, lines[j].icmp);
                            if (send(client[i].fd, buf, length, 0) == -1)
                                syserr("send");
                        }
                    }

                    gettimeofday(&timeNow, NULL);
                    connected[i].lastWrote = (uint64_t) timeNow.tv_sec * MICRO +
                        (uint64_t) timeNow.tv_usec;

                    //printf("i = %d ustawiam lastwrote na %" PRIu64 "\n", i, connected[i].lastWrote);
                }
            }

        }
    }
}

uint16_t getSeq()
{
    return icmpSeq++;
}

void icmpResponser(void * arg)
{
    char rcvBuffer[BSIZE];
    ssize_t ipHeaderLen = 0, dataLen = 0, icmpLen = 0, len = 0;
    struct sockaddr_in rcvAddr;
    socklen_t rcvAddrLen;
    struct icmp* icmp;
    int index;
    struct timeval timeNow;
    uint64_t diff;

    memset(rcvBuffer, 0, sizeof(rcvBuffer));
    rcvAddrLen = (socklen_t) sizeof(rcvAddr);

    icmp = (struct icmp *) rcvBuffer;
    icmp->icmp_type = 0;
    icmp->icmp_id = 0;
    icmp->icmp_seq = 0;

    while (icmpLen < ICMP_HEADER_LEN ||
            icmp->icmp_type != ICMP_ECHOREPLY ||
            ntohs(icmp->icmp_id) != 0x13)
    {
        len = recvfrom(rawSock, (void *) rcvBuffer, sizeof(rcvBuffer), 0,
                (struct sockaddr *) &rcvAddr, &rcvAddrLen);
        struct ip* ip;
        ip = (struct ip *) rcvBuffer;
        ipHeaderLen = ip->ip_hl << 2;
        icmp = (struct icmp *) (rcvBuffer + ipHeaderLen);
        icmpLen = len - ipHeaderLen;
        gettimeofday(&timeNow, NULL);
    }

    //a proper packet is received

    if (pthread_mutex_lock(&icmpMutex) != 0)
        syserr("pthread_mutex_lock1");

    index = findIcmpIndex(ntohs(icmp->icmp_seq));
    if (index == -1)
        printf("strange seq number\n");
    else
    {
        diff = (uint64_t) timeNow.tv_sec * MICRO +
            (uint64_t) timeNow.tv_usec;
        diff -= icmpTime[index];
        lags[icmpIndex[index]].icmp[lags[icmpIndex[index]].icmpI] = diff;
        lags[icmpIndex[index]].icmpI++;
        lags[icmpIndex[index]].icmpI %= 10;

        printf("Dostalem odpowiedz ICMP, wyszlo %" PRIu64 "\n", diff);

        sentIcmp[index] = -1;
        icmpTime[index] = 0;
        icmpIndex[index] = -1;
    }

    if (pthread_mutex_unlock(&icmpMutex) != 0)
        syserr("pthread_mutex_unlock");
}

void checkIcmpLag(void * arg)
{
    printf("ROBIE TEST ICMP\n");
    struct lagParams par = *((struct lagParams*) arg);

    struct sockaddr_in sendAddr;
    char sendBuffer[BSIZE];
    struct icmp* icmp;
    ssize_t dataLen = 0, icmpLen = 0, len = 0;
    struct timeval tvBefore, tvAfter;
    uint16_t seqNum;

    if (pthread_mutex_lock(par.protect) != 0)
        syserr("pthread_mutex_lock");

    sendAddr.sin_family = AF_INET;
    sendAddr.sin_addr.s_addr = nodes[par.index].ip;
    sendAddr.sin_port = htons(0);

    memset(sendBuffer, 0, sizeof(sendBuffer));
    icmp = (struct icmp *) sendBuffer;
    icmp->icmp_type = ICMP_ECHO;
    icmp->icmp_code =  0;
    icmp->icmp_id = htons(0x13);
    seqNum = getSeq();
    icmp->icmp_seq = htons(seqNum);
    //set icmp_data here

    dataLen = snprintf(((char *) sendBuffer + ICMP_HEADER_LEN),
            sizeof(sendBuffer) - ICMP_HEADER_LEN, "BASIC PING!");
    if (dataLen < 1)
        syserr("snprintf");

    icmpLen = dataLen + ICMP_HEADER_LEN;
    icmp->icmp_cksum = 0;
    icmp->icmp_cksum = in_cksum((unsigned short*) icmp, icmpLen);

    if (pthread_mutex_unlock(par.protect) != 0)
        syserr("pthread_mutex_unlock");

    if (pthread_mutex_lock(&icmpMutex) != 0)
        syserr("pthread_mutex_lock");

    int index = findFreeIcmpIndex();

    if (pthread_mutex_unlock(&icmpMutex) != 0)
        syserr("pthread_mutex_unlock");

    if (index == -1)
    {
        printf("Too many ICMP packets, aborting test\n");
        return;
    }

    gettimeofday(&tvBefore, NULL);

    len = sendto(rawSock, (void *) icmp, icmpLen, 0, (struct sockaddr *) &sendAddr,
            (socklen_t) sizeof(sendAddr));
    if ((ssize_t) len != icmpLen)
        syserr("sendto");

    if (pthread_mutex_lock(&icmpMutex) != 0)
        syserr("pthread_mutex_lock");

    sentIcmp[index] = seqNum;
    icmpTime[index] = (uint64_t) tvBefore.tv_sec * MICRO +
        (uint64_t) tvBefore.tv_usec;
    icmpIndex[index] = par.index;

    if (pthread_mutex_unlock(&icmpMutex) != 0)
        syserr("pthread_mutex_unlock");

    icmpResponser(NULL);
}

void checkUdpLag(void * arg)
{
    printf("ROBIE TEST UDP ");
    struct lagParams par = *((struct lagParams*) arg);
    printf("DLA INDEKSU %d\n", par.index);
    int sock, flags = 0;
    uint64_t mesg, ans[2];
    ssize_t recv, len, sent;
    struct timeval tvBefore, tvAfter;
    struct sockaddr_in myAddr, srvAddr;
    socklen_t rcva;

    myAddr.sin_family = AF_INET;
    myAddr.sin_port = htons(par.udpPort);

    sock = socket(PF_INET, SOCK_DGRAM, 0);
    if (sock < 0)
        syserr("socket");

    if (pthread_mutex_lock(par.protect) != 0)
        syserr("pthread_mutex_lock");

    myAddr.sin_addr.s_addr = nodes[par.index].ip;
    recv = (socklen_t) sizeof(myAddr);
    len = sizeof(uint64_t);

    gettimeofday(&tvBefore, NULL);
    mesg = ((uint64_t) tvBefore.tv_sec) * MICRO + ((uint64_t) tvBefore.tv_usec);
    mesg = htobe64(mesg);
    sent = sendto(sock, &mesg, len, flags, (struct sockaddr *) &myAddr, recv);
    if (sent != (int) len)
        syserr("sendto");

    flags = 0;
    len = (size_t) sizeof(ans);
    rcva = (socklen_t) sizeof(srvAddr);
    recv = recvfrom(sock, ans, len, flags, (struct sockaddr *) &srvAddr, &rcva);
    gettimeofday(&tvAfter, NULL);
    if (recv < 0)
        syserr("recvfrom");

    if (recv != sizeof(uint64_t) * 2)
    {
        if (close(sock) == -1)
            syserr("close");
        return;
    }

    uint64_t diff = ((uint64_t) tvAfter.tv_sec) * MICRO + ((uint64_t) tvAfter.tv_usec) -
        (((uint64_t) tvBefore.tv_sec) * MICRO + ((uint64_t) tvBefore.tv_usec));
    lags[par.index].udp[lags[par.index].udpI] = diff;
    lags[par.index].udpI++;
    lags[par.index].udpI %= 10;

    //printf("Test udp dla %d wyszlo %" PRIu64 "\n", nodes[par.index].ip, diff);

    if (pthread_mutex_unlock(par.protect) != 0)
        syserr("pthread_mutex_unlock");

    if (close(sock) == -1)
        syserr("close");
}

void checkTcpLag(void * arg)
{
    printf("ROBIE TEST TCP\n");
    struct lagParams par = *((struct lagParams*) arg);

    struct sockaddr_in dest;
    int sock;
    struct timeval tvBefore, tvAfter;

    sock = socket(PF_INET, SOCK_STREAM, 0);
    if (sock < 0)
        syserr("socket");

    if (pthread_mutex_lock(par.protect) != 0)
        syserr("lock");

    memset(&dest, 0, sizeof(dest));
    dest.sin_family = AF_INET;
    dest.sin_addr.s_addr = nodes[par.index].ip;
    dest.sin_port = htons(tcpPort);

    gettimeofday(&tvBefore, NULL);
    connect(sock, (struct sockaddr *) &dest, sizeof(struct sockaddr));
    gettimeofday(&tvAfter, NULL);


    uint64_t diff = ((uint64_t) tvAfter.tv_sec) * MICRO + ((uint64_t) tvAfter.tv_usec) -
        (((uint64_t) tvBefore.tv_sec) * MICRO + ((uint64_t) tvBefore.tv_usec));

    lags[par.index].tcp[lags[par.index].tcpI] = diff;

    lags[par.index].tcpI++;
    lags[par.index].tcpI %= 10;

    printf("Test tcp dla %d wyszlo %" PRIu64 "\n", nodes[par.index].ip, diff);

    if (pthread_mutex_unlock(par.protect) != 0)
        syserr("unlock");

    if (close(sock) == -1)
        syserr("close");
}

void checkLag(void * arg)
{
    int i, counter;
    struct parameters params = *((struct parameters*) arg);
    pthread_t lagTcpWorker[N], lagUdpWorker[N], lagIcmpWorker[N];
    pthread_attr_t lagTcpAttr[N], lagUdpAttr[N], lagIcmpAttr[N];
    struct lagParams lagPar[N];

    while(1)
    {
        counter = 0;
        for (i = 0; i < N; i++)
        {
            if (pthread_mutex_lock(&mutex[i]) != 0)
                syserr("lock");

            if (nodes[i].ip != -1)
            {
                lagPar[i].index = i;
                lagPar[i].protect = &mutex[i];
                lagPar[i].udpPort = params.udpLatencyPort;

                counter++;
		        printf("widze typa o ip: %d pod indeksem %d\n", nodes[i].ip, i);
                if (nodes[i].tcp == 1)
                {
                    //run TCP lag test
                    printf("ODPALAM WATEK TCP_____________\n");
                    if (pthread_attr_init(&lagTcpAttr[i]) != 0)
                        syserr("pthread_attr_init");

                    if (pthread_attr_setdetachstate(&lagTcpAttr[i],
                            PTHREAD_CREATE_DETACHED) != 0)
                        syserr("pthread_attr_setdetachstate");

                    if (pthread_create(&lagTcpWorker[i], &lagTcpAttr[i],
                        (void *) checkTcpLag, (void *) &lagPar[i]) != 0)
                        syserr("pthread_create");
                }
                //run UDP test
		        printf("odpalam UDP dla indeksu %d\n", i);
                if (pthread_attr_init(&lagUdpAttr[i]) != 0)
                    syserr("pthread_attr_init");

                if (pthread_attr_setdetachstate(&lagUdpAttr[i],
                        PTHREAD_CREATE_DETACHED) != 0)
                    syserr("pthread_attr_setdetachstate");

                if (pthread_create(&lagUdpWorker[i], &lagUdpAttr[i],
                    (void *) checkUdpLag, (void *) &lagPar[i]) != 0)
                    syserr("pthread_create");

                //run ICMP test
                if (pthread_attr_init(&lagIcmpAttr[i]) != 0)
                    syserr("pthread_attr_init");

                if (pthread_attr_setdetachstate(&lagIcmpAttr[i],
                            PTHREAD_CREATE_DETACHED) != 0)
                    syserr("pthread_attr_setdetachstate");

                if (pthread_create(&lagIcmpWorker[i], &lagUdpAttr[i],
                    (void *) checkIcmpLag, (void *) &lagPar[i]) != 0)
                    syserr("pthread_create");

                //unlock mutex so threads can write
                if (pthread_mutex_unlock(&mutex[i]) != 0)
                    syserr("unlock");

            }
            else
            {
                if (pthread_mutex_unlock(&mutex[i]) != 0)
                    syserr("unlock");
            }
        }
        printf("ZBADALEM %d HOSTOW!!!!!!!!!\n", counter);
        sleep(params.checkLagDelay);
    }
}

int main(int argc, char* argv[])
{
    pthread_t workers[threadNumber];
    pthread_attr_t workerAttrs[threadNumber]; //set to deteached!
    struct parameters params;
    int i;

    init();
    ///
    /*nodes[0].ip = inet_addr("127.0.0.1");
    nodes[0].tcp = 1;

    lags[0].ip = nodes[0].ip;
    lags[0].tcp[0] = 5;
    lags[0].tcp[1] = 5;
    lags[0].tcp[2] = 9;
    */
    ///
    setFlags(argc, argv, &params);

    struct dnsCliArgs cliArgs;
    cliArgs.nodesArray = nodes;
    cliArgs.mutexArray = mutex;
    cliArgs.scanInterval = params.searchDelay;
    cliArgs.countHosts = &lastTakenNode;

    for (i = 0; i < threadNumber; i++)
    {
        if (pthread_attr_init(&workerAttrs[i]) != 0)
            syserr("pthread_attr_init");
        if (pthread_attr_setdetachstate(&workerAttrs[i],
                    PTHREAD_CREATE_DETACHED) != 0)
            syserr("pthread_attr_setdetachedstate");
    }

    //start 5 threads for being server/client
    if (pthread_create(&workers[0], &workerAttrs[0],
                (void *) hostUdpLag, (void *) &params) != 0)
        syserr("pthread_create");
    if (pthread_create(&workers[1], &workerAttrs[1],
                (void *) checkLag, (void *) &params) != 0)
        syserr("pthread_create");
    if (pthread_create(&workers[2], &workerAttrs[2],
                (void *) dnsServer, (void *) &params) != 0)
        syserr("pthread_create");
    if (pthread_create(&workers[3], &workerAttrs[3],
                (void *) dnsClient, (void *) &cliArgs) != 0)
        syserr("pthread_create");

    if (pthread_create(&workers[4], &workerAttrs[4],
                (void *) UIServ, (void *) &params) != 0)
        syserr("pthread_create");

    //do nothing, let threads do the work
    pause();

    return 0;
}
