#ifndef _IN_CKSUM_H_
#define _IN_CKSUM_H_

/* source: libfree */

unsigned short in_cksum(unsigned short *addr, int len);

#endif
