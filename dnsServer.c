#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <poll.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdlib.h>
#include <pthread.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "helpers.h"
#include "err.h"
#include "types.h"

#define LISTENER_COUNT 1
#define SERV_NAME "MD_DNS"

void partitionDnsSrv(int* index, char* buf, const char* data)
{
    buf[(*index)++] = strlen(data);
    memcpy(buf + *index, data, strlen(data));
    (*index) += strlen(data);
}

char* createQueryASrv(int* index, const char* name, const char* isSSH,
        const char* commType)
{
    char* buf = (char*) malloc(DNS_SIZE);
    struct DNS_HEADER *dns = (struct DNS_HEADER *)buf;
    dns->id = 0;
    dns->qr = 0;
    dns->opcode = 0;
    dns->aa = 0;
    dns->tc = 0;
    dns->rd = 1;
    dns->ra = 0;
    dns->z = 0;
    dns->ad = 0;
    dns->cd = 0;
    dns->rcode = 0;
    dns->q_count = htons(1);
    dns->ans_count = 0;
    dns->auth_count = 0;
    dns->add_count = 0;

    *index = sizeof(struct DNS_HEADER);

    partitionDnsSrv(index, buf, name);
    partitionDnsSrv(index, buf, isSSH);
    partitionDnsSrv(index, buf, commType);
    partitionDnsSrv(index, buf, "local");

    buf[(*index)++] = 0;

    struct QUESTION *quest = (struct QUESTION *)(buf+*index);
    quest->qtype = htons(A);
    quest->qclass = htons(1);
    (*index) = *index + sizeof(struct QUESTION);
    return buf;
}

int parseQueryData(const char* buf, const int len, const char* servName,
        int* qLen)
{
    char qname[NAME_PARTS][NAME_LEN];
    int qname_parts = 0;
    int i, j;

    for(i = 0; i < NAME_PARTS; i++)
    {
        for(j = 0; j < NAME_LEN; j++)
            qname[i][j] = 0;
    }

    int pos = sizeof(struct DNS_HEADER);

    while(1)
    {
        if(pos >= len)
            break;

        if (buf[pos] == 0)
            break;

        if(buf[pos] + pos >= len || buf[pos] >= NAME_LEN)
            break;
        
        memcpy(qname[qname_parts++], buf + pos + 1, buf[pos]);
        
        pos += buf[pos];
        pos++;

        (*qLen) = pos + 1 - sizeof(struct DNS_HEADER);
    }

    struct QUESTION *ques = (struct QUESTION *)(buf + pos + 1);
    int qtype = ntohs(ques->qtype);
    
    short isOp, isUdp, isSsh, isLocal, isTcp, isNameOk;
    
    if (qname_parts == 3 && qtype == PTR)
    {
        isOp = (strcmp(qname[0], "_opoznienia") == 0);
        isUdp = (strcmp(qname[1], "_udp") == 0);
        isLocal = (strcmp(qname[2], "local") == 0);
        isSsh = (strcmp(qname[0], "_ssh") == 0); //best variable name
        isTcp = (strcmp(qname[1], "_tcp") == 0);

        if (isSsh && isLocal && isTcp)
            return SSH_QUERY_PTR;
        else if (isOp && isUdp && isLocal)
            return UDP_QUERY_PTR;
    }
    
    else if (qname_parts == 4 && qtype == A)
    {
        isNameOk = (strcmp(qname[0], servName) == 0);
        isOp = (strcmp(qname[1], "_opoznienia") == 0);
        isUdp = (strcmp(qname[2], "_udp") == 0);
        isLocal = (strcmp(qname[3], "local") == 0);
        isTcp = (strcmp(qname[2], "_tcp") == 0);
        isSsh = (strcmp(qname[1], "_ssh") == 0);
        
        if (isNameOk)
        {
            if (isSsh && isTcp && isLocal)
                return SSH_QUERY_A;
            else if (isOp && isLocal && isUdp)
                return UDP_QUERY_A;
        }
    }
    return UNKNOWN;
}

int createAnswer(char* result, const char* body, int dnsLen,
        const char* selfName, int serIPAd, int tcp)
{
    int qNamLen;
    int mesg;
    
    memcpy(result, body, sizeof(struct DNS_HEADER));
    mesg = parseQueryData(body, dnsLen, selfName, &qNamLen);
    
    if(mesg == UNKNOWN)
        return 0;

    struct DNS_HEADER *d = (struct DNS_HEADER*) result;
    d->q_count = 0;
    d->ans_count = htons(1);
    d->qr = 1;

    //name stays the same
    memcpy(result + sizeof(struct DNS_HEADER),
            body + sizeof(struct DNS_HEADER), qNamLen);

    //pick rdata
    //for A
    if(mesg == UDP_QUERY_A || mesg == SSH_QUERY_A)
    {
        struct R_DATA* r = (struct R_DATA*)
            (result + sizeof(struct DNS_HEADER) + qNamLen); //set rdata parts
        r->ttl = 0;
        r->type = htons(A);
        r->_class = htons(1);
        r->data_len = htons(sizeof(serIPAd));

        memcpy(result + qNamLen + sizeof(struct DNS_HEADER) +
                sizeof(struct R_DATA) - 2, &serIPAd, sizeof(serIPAd));
        
        return sizeof(struct DNS_HEADER) + qNamLen + sizeof(struct R_DATA) +
            sizeof(serIPAd) - 2;
    }
    
    //same for PTR
    if(mesg == SSH_QUERY_PTR || mesg == UDP_QUERY_PTR)
    {
        if (mesg == SSH_QUERY_PTR &&
                tcp == 1)
            return 0;

        struct R_DATA* r = (struct R_DATA*) (result +
                sizeof(struct DNS_HEADER) + qNamLen);
        r->_class = htons(1);
        r->type = htons(PTR);
        r->ttl = 0;
        r->data_len = htons(strlen(selfName));
        
        memcpy(result + sizeof(struct DNS_HEADER) + qNamLen +
                sizeof(struct R_DATA) - 2, selfName, strlen(selfName));
        
        return sizeof(struct DNS_HEADER) + qNamLen +
            sizeof(struct R_DATA) + strlen(selfName) - 2;
    }
}

//source: stackOverflow 5281409
int recvFrom(int s, void *buf, size_t len, int flags,
        struct sockaddr_in *from, int* server_ip)
{
    // sock is bound AF_INET socket, usually SOCK_DGRAM
    // include struct in_pktinfo in the message "ancilliary" control data
    // the control data is dumped here
    char cmbuf[DNS_SIZE];
    // the remote/source sockaddr is put here
    struct sockaddr_in peeraddr;
    struct iovec iov[1];
    iov[0].iov_base = buf;
    iov[0].iov_len = DNS_SIZE;
    // if you want access to the data you need to init the msg_iovec fields
    struct msghdr mh = {
        .msg_name = &peeraddr,
        .msg_namelen = sizeof(peeraddr),
        .msg_iov=iov,
        .msg_iovlen=1,
        .msg_control=cmbuf,
        .msg_controllen=DNS_SIZE
    };
    struct cmsghdr *cmsg = CMSG_FIRSTHDR(&mh);
    int count = recvmsg(s, &mh, 0);
    if (count == -1)
        syserr("recvmsg");
    for ( // iterate through all the control headers
        ;
        cmsg != NULL;
        cmsg = CMSG_NXTHDR(&mh, cmsg))
    {
        // ignore the control headers that don't match what we want
        if (cmsg->cmsg_level != IPPROTO_IP ||
            cmsg->cmsg_type != IP_PKTINFO)
        {
            continue;
        }
        struct in_pktinfo *pi = CMSG_DATA(cmsg);
        (*from) =  peeraddr;
        (*server_ip) = pi->ipi_spec_dst.s_addr;
    }
    return count;
}

void* dnsServer(void * arg)
{
    printf("DNS SERVER startup\n");

    struct parameters args = *(struct parameters *) arg;
    int id, sock;
    int sockBin;
    struct sockaddr_in servAddr;
    char servName[30] = SERV_NAME;

    int servIP = 0;
    int ssh_status = args.sshTcpOn;
	int port = DNS_PORT, fass;
    int recvs, len;

    sock = socket(PF_INET, SOCK_DGRAM, 0);
    if (sock < 0)
        syserr("socket");

    struct pollfd listener[LISTENER_COUNT];
    listener[0].fd = sock;
    listener[0].events = POLLIN;
    listener[0].revents = 0;
    
    id = 0;

    printf("DNS SERVER getting name\n");
    while(1) 
    {
        id++;
        int lnam = strlen(SERV_NAME);
        int id2 = id;
        
        while(1) 
        {
            servName[lnam++] = (id2 % 10) + (int) '0'; //append
            id2 /= 10;

            if (id2 == 0)
                break;
        }
        printf("DNS SERVER try %s %d\n", servName, id);
        
        char* mesg = createQueryASrv(&len, servName, "_opoznienia", "_udp");
        sendDNS(sock, port, mesg, len);
        free(mesg);

        if (poll(listener, LISTENER_COUNT, 1500) == -1)
            syserr("poll");
        
        if (listener[0].revents & POLLIN)
        {
            listener[0].revents = 0;
            char dns[DNS_SIZE];
            printf("%d\n", recv(sock, dns, DNS_SIZE, 0));
        }
        else
        {
            printf("DNS SERVER start %s\n", servName);
            close(sock);
            break;
        }
    }

    sockBin = socket(PF_INET, SOCK_DGRAM, 0);
    if (sockBin < 0)
        syserr("socket");

    servAddr.sin_family = AF_INET;
    servAddr.sin_port = htons(port);
    servAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    fass = 1;
    if (setsockopt(sockBin, SOL_SOCKET, SO_REUSEADDR, &fass, sizeof(fass)) == -1)
        syserr("setsockopt");
    if (setsockopt(sockBin, IPPROTO_IP, IP_PKTINFO, &fass, sizeof(fass)) == -1)
        syserr("setsockopt");
    if (bind(sockBin, (struct sockaddr *) &servAddr, (socklen_t) sizeof(struct sockaddr)) == -1)
        syserr("bind");
    int binded = sockBin;

    printf("DNS SERVER working\n");
	
    while (1)
    {
        char dns[DNS_SIZE], reply[DNS_SIZE];
        struct sockaddr_in client_address;

        socklen_t addr_len;

        len = recvFrom(binded, dns, sizeof(dns), 0, &client_address, &servIP);
        
        addr_len = sizeof(client_address);

        recvs = createAnswer(reply, dns, len, servName, servIP, ssh_status);

        if (recvs > 0)
        {
            if (sendto(binded, reply, recvs, 0, (struct sockaddr *) &client_address, addr_len) == -1)
                syserr("sendto");
        }
	}
}
