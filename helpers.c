#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <poll.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdlib.h>
#include <pthread.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "helpers.h"

#define MICRO 1000000

int sendDNS(int sock, int port, char* mesg, int count)
{
    struct sockaddr_in servaddr;
    int flags;
    flags = 0;

    servaddr.sin_addr.s_addr = inet_addr("224.0.0.251");
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(port);
    return sendto(sock, mesg, count, flags, (struct sockaddr *) &servaddr, sizeof(struct sockaddr));
}

int timeDiff(struct timeval before, struct timeval after)
{
    uint64_t bef = (uint64_t) before.tv_sec * MICRO + (uint64_t) before.tv_usec;
    uint64_t aft = (uint64_t) after.tv_sec * MICRO + (uint64_t) after.tv_usec;

    if (aft > bef) //in case someone forgets the order...
        return (int) (aft - bef);
    return (int) (bef - aft);
}
