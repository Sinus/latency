#ifndef _HELPERS_H_
#define _HELPERS_H_

int sendDNS(int, int, char*, int);
int timeDiff(struct timeval, struct timeval);

#endif
