#ifndef _TYPES_H_
#define _TYPES_H_

#include <stdint.h>
#include <pthread.h>

#define N 10000
#define LAG_CACHE 10
#define PTR 12
#define A 1

#define DNS_PORT 5353
#define NAME_PARTS 4
#define NAME_LEN 255
#define DNS_SIZE 63555
#define UNKNOWN -1
#define SSH_QUERY_A 1
#define SSH_QUERY_PTR 2
#define UDP_QUERY_A 3
#define UDP_QUERY_PTR 4

#define SSH_ANSWER_A SSH_QUERY_A
#define SSH_ANSWER_PTR SSH_QUERY_PTR 
#define UDP_ANSWER_A UDP_QUERY_A 
#define UDP_ANSWER_PTR UDP_QUERY_PTR 

struct parameters
{
    int udpLatencyPort; // -u
    int uiPort; // -U
    int checkLagDelay; // -t
    int searchDelay; // -T
    int uiDelay; // -v
    short sshTcpOn; // -s
};

struct nodeInfo
{
    int ip;
    int tcp;
};

struct lagInfo
{
    int ip;
    int icmpI;
    int tcpI;
    int udpI;
    uint64_t icmp[LAG_CACHE];
    uint64_t tcp[LAG_CACHE];
    uint64_t udp[LAG_CACHE];
};

struct lagParams
{
    int index;
    int udpPort;
    pthread_mutex_t *protect;
};

struct uiClient
{
    int fd;
    short active;
    uint64_t lastWrote;
};

struct line
{
    int ip;
    int udp;
    int tcp;
    int icmp;
};

struct dnsCliArgs
{
    struct nodeInfo* nodesArray;
    pthread_mutex_t* mutexArray;
    int scanInterval;
    int* countHosts; //ostatni zajety numer (0)
};

struct clientView
{
    int writeFrom;
};

// DNS structs source: http://www.binarytides.com/dns-query-code-in-c-with-linux-sockets/

struct DNS_HEADER
{
unsigned short id;       // identification number
unsigned char rd :1;     // recursion desired
unsigned char tc :1;     // truncated message
unsigned char aa :1;     // authoritive answer
unsigned char opcode :4; // purpose of message
unsigned char qr :1;     // query/response flag
unsigned char rcode :4;  // response code
unsigned char cd :1;     // checking disabled
unsigned char ad :1;     // authenticated data
unsigned char z :1;      // its z! reserved
unsigned char ra :1;     // recursion available
unsigned short q_count;  // number of question entries
unsigned short ans_count; // number of answer entries
unsigned short auth_count; // number of authority entries
unsigned short add_count; // number of resource entries
};

struct QUESTION
{
unsigned short qtype;
unsigned short qclass;
};

struct R_DATA
{
unsigned short type;
unsigned short _class;
unsigned int ttl;
unsigned short data_len;
};

typedef struct
{
unsigned char *name;
struct R_DATA *resource;
unsigned char *rdata;
} RES_RECORD;

#endif
